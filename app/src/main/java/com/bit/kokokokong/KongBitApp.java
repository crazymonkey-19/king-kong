package com.bit.kokokokong;

import android.app.Application;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class KongBitApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public static KingBitApi getApi() {
        Retrofit retrofitRefresh = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofitRefresh.create(KingBitApi.class);
    }
}
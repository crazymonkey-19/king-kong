package com.bit.kokokokong;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.facebook.FacebookSdk;
import com.onesignal.OneSignal;

import java.net.HttpURLConnection;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;

import bolts.AppLinks;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {


    @BindView(R.id.webView)
    WebView webView;
    private String deepLinkEndPoint;
    private SharedPreferenceUtils mSharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();
        mSharedPreferences = SharedPreferenceUtils.getInstance(this);
        initFacebook();
        openWebViewWhenUrlExist();
    }

    @OnClick(R.id.btnPlay)
    void onClickPlay() {
        String newUrl = mSharedPreferences.getStringValue("new_url", "");
        if (!Objects.equals(newUrl, "")) {
            initVebView(newUrl);
        } else {
            startActivity(new Intent(this, GameActivity.class));
        }
    }

    @OnClick(R.id.btnDemo)
    void onClickDemo() {
        startActivity(new Intent(this, GameActivity.class));
    }

    private void initFacebook() {
        FacebookSdk.setAutoInitEnabled(true);
        FacebookSdk.fullyInitialize();
        TelephonyManager tm = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        if (tm != null) {
            Log.i("tagik", "initFacebook: " + tm.getNetworkCountryIso());
            setupDeepLink(AppLinks.getTargetUrlFromInboundIntent(this, getIntent()),
                    tm.getNetworkCountryIso());
        } else {
            setupDeepLink(AppLinks.getTargetUrlFromInboundIntent(this, getIntent()),
                    "XX");
        }
    }

    private void initVebView(String uri) {
        webView.setVisibility(View.VISIBLE);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.loadUrl(uri);
        webView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                cachingUrl(url);
                view.loadUrl(url);
                return true;
            }
        });
    }


    private void setupDeepLink(Uri targetUri, String countryCode) {
        if (targetUri != null && targetUri.getEncodedPath() != null
                && targetUri.getEncodedPath().length() > 1) {
            String endPoint = targetUri.getEncodedPath().substring(1);
            String[] key = endPoint.split(Pattern.quote("$$"));
            if (key.length > 0) {
                getUrl(countryCode, key[0]);
            }
            if (key.length > 1) {
                deepLinkEndPoint = key[1];
            }
        }
    }

    private void openWebViewWhenUrlExist() {
        String newUrl = mSharedPreferences.getStringValue("new_url", "");
        if (!Objects.equals(newUrl, "")) {
            initVebView(newUrl);
        } else {
            TelephonyManager tm = (TelephonyManager) getApplicationContext().getSystemService(TELEPHONY_SERVICE);
            if (tm != null) {
                getUrl(tm.getNetworkCountryIso(), "");
            } else {
                getUrl("XX", "");
            }
        }
    }

    private void cachingUrl(String url) {
        if (url != null) {
            mSharedPreferences.setValue("new_url", url);
        }
    }

    private void getUrl(String countryCode, String key) {
        KongBitApp.getApi().getUrl(countryCode, key).enqueue(new Callback<List<String>>() {
            @Override
            public void onResponse(@NonNull Call<List<String>> call, @NonNull Response<List<String>> response) {
                if (response.isSuccessful() && response.code()
                        == HttpURLConnection.HTTP_OK && response.body() != null) {
                    List<String> urls = response.body();
                    StringBuilder url = new StringBuilder();
                    if (urls.size() > 0) {
                        url.append(urls.get(0));
                        url.append("&geta=");
                        url.append(getApplicationContext().getPackageName());
                        url.append("&getb=");
                        url.append(deepLinkEndPoint);
                        initVebView(url.toString());
                    }
                    Log.i("tagik", "onResponse: " + response.body());
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<String>> call, @NonNull Throwable t) {
                Log.e("Failure", "onFailure: ", t);
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (webView.getVisibility() == View.VISIBLE) {
            webView.setVisibility(View.GONE);
        } else {
            super.onBackPressed();
        }
    }
}

package com.bit.kokokokong;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GameActivity extends AppCompatActivity {

    private int price;

    @BindView(R.id.txtPrice)
    TextView txtPrice;

    @BindView(R.id.view)
    View view;


    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        ButterKnife.bind(this);
        GridView gridview = findViewById(R.id.gridview);
        gridview.setOnTouchListener((v, event) -> event.getAction() == MotionEvent.ACTION_MOVE);
        ImageAdapter imageAdapter = new ImageAdapter(this);
        imageAdapter.setGameEndListener(new ImageAdapter.GameEndListener() {
            @Override
            public void onGameEnded() {
                price = 0;
                setText(price);
                openDialog(price, "You Los!");
            }

            @Override
            public void victory() {
                price *= 2;
                setText(price);
            }

            @Override
            public void winner() {
                openDialog(price, "You Win!");
            }
        });
        gridview.setAdapter(imageAdapter);
    }

    @OnClick(R.id.view)
    void onClickView() {
        Toast.makeText(this, "Click PLAY!", Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.btnPlayGame)
    void onClickPlay() {
        if (price == 0) {
            Toast.makeText(this, "Вставте ставку", Toast.LENGTH_SHORT).show();
        } else {
            view.setVisibility(View.GONE);
        }
    }


    @OnClick(R.id.btnPlus)
    void onClickPlus() {
        if (price == 0 || price == 1 || price == 2 || price == 3 || price == 4) {
            price += 1;
            setText(price);
        } else if (price == 5) {
            price *= 2;
            setText(price);
        } else if (price == 10) {
            price += 15;
            setText(price);
        } else if (price == 25) {
            price *= 2;
            setText(price);
        } else if (price == 50) {
            price *= 2;
            setText(price);
        }
    }

    private void setText(int bit) {
        txtPrice.setText(getResources().getString(R.string.bitcoin, bit));
    }

    @SuppressLint("SetTextI18n")
    private void openDialog(int score, String text) {
        PlayAgainDialog playAgainDialog = new PlayAgainDialog(this, score, text);
        Objects.requireNonNull(playAgainDialog.getWindow())
                .setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        playAgainDialog.setOnClickListener((dialog, which) -> {
            playAgainDialog.cancel();
            startActivity(new Intent(GameActivity.this, GameActivity.class));
        });
        playAgainDialog.setCancelable(false);
        playAgainDialog.show();
    }
}

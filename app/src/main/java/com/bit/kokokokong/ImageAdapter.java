package com.bit.kokokokong;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ImageAdapter extends BaseAdapter {
    private Context mContext;
    private Integer[] pieces;
    private List<ImageView> imageViews;
    private int piece_up = -1;
    private int mCountOfOk;
    private GameEndListener mGameEndListener;
    int victoryCount;

    void setGameEndListener(GameEndListener mGameEndListener) {
        this.mGameEndListener = mGameEndListener;
    }

    ImageAdapter(Context c) {
        mContext = c;
        List<Integer> ipieces = new ArrayList<>();
        for (int i = 0; i < 27; i++) {
            ipieces.add(i);
            ipieces.add(i);
        }
        Collections.shuffle(ipieces);
        pieces = ipieces.toArray(new Integer[0]);
        _createImageViews();
    }

    private void _createImageViews() {
        imageViews = new ArrayList<>();
        for (int position = 0; position < getCount(); position++) {
            ImageView imageView;
            imageView = new ImageView(mContext);
            imageView.setLayoutParams(new GridView.LayoutParams(90, 90));
            imageView.setPadding(1, 5, 1, 5);
            imageView.setImageResource(R.drawable.card);
            imageViews.add(imageView);
            installClick(position);
        }
    }

    public int getCount() {
        return 27; //mThumbIds.length;
    }

    public Object getItem(int position) {
        return imageViews.get(position);
    }

    public long getItemId(int position) {
        return pieces[position].longValue();
    }

    // create a new ImageView for each item referenced by the Adapter
    public synchronized View getView(int position, View convertView, ViewGroup parent) {
        return imageViews.get(position);
    }

    private void installClick(int position) {
        final ImageAdapter self = this;
        ImageView imageView = imageViews.get(position);
        imageView.setOnClickListener(v -> {
            int pos = imageViews.indexOf(v);
            show(pos);
            if (piece_up == -1 || piece_up == pos) {
                // first click
                piece_up = pos;
            } else {
                // second click
                if (pieces[pos].equals(pieces[piece_up])) {
                    mCountOfOk++;
                    // remove click handler
                    removeClick(pos);
                    removeClick(piece_up);
                }
                piece_up = -1;
            }
            if (pos >= 24 && pos <= 26) {
                victoryCount += 1;
                removeClick(24);
                removeClick(25);
                removeClick(26);
            } else if (pos >= 21 && pos <= 23) {
                victoryCount += 1;
                removeClick(21);
                removeClick(22);
                removeClick(23);
            } else if (pos >= 18 && pos <= 20) {
                victoryCount += 1;
                removeClick(18);
                removeClick(19);
                removeClick(20);
            } else if (pos >= 15 && pos <= 17) {
                victoryCount += 1;
                removeClick(15);
                removeClick(16);
                removeClick(17);
            } else if (pos >= 12 && pos <= 14) {
                victoryCount += 1;
                removeClick(12);
                removeClick(13);
                removeClick(14);
            } else if (pos >= 9 && pos <= 11) {
                removeClick(9);
                removeClick(10);
                removeClick(11);
            } else if (pos >= 6 && pos <= 8) {
                victoryCount += 1;
                removeClick(6);
                removeClick(7);
                removeClick(8);
            } else if (pos >= 3 && pos <= 5) {
                victoryCount += 1;
                removeClick(3);
                removeClick(4);
                removeClick(5);
            } else if (pos >= 0 && pos <= 2) {
                victoryCount += 1;
                removeClick(0);
                removeClick(1);
                removeClick(2);
            }
        });

    }

    private void removeClick(int position) {
        ImageView aux;
        aux = imageViews.get(position);
        aux.setOnClickListener(null);
    }

    private void show(int position) {
        ImageView img;
        img = imageViews.get(position);
        int piece = pieces[position];
        img.setImageResource(mThumbIds[piece]);
        if (mThumbIds[piece] == R.drawable.x) {
            mGameEndListener.onGameEnded();
        } else if (victoryCount == 8) {
            mGameEndListener.winner();
        } else {
            mGameEndListener.victory();
        }
    }

    // references to our images
    private Integer[] mThumbIds = {
            R.drawable.check,
            R.drawable.x,
            R.drawable.check,
            R.drawable.check,
            R.drawable.check,
            R.drawable.x,
            R.drawable.check,
            R.drawable.check,
            R.drawable.check,
            R.drawable.x,
            R.drawable.check,
            R.drawable.x,
            R.drawable.check,
            R.drawable.check,
            R.drawable.check,
            R.drawable.x,
            R.drawable.check,
            R.drawable.check,
            R.drawable.check,
            R.drawable.x,
            R.drawable.check,
            R.drawable.x,
            R.drawable.check,
            R.drawable.x,
            R.drawable.check,
            R.drawable.check,
            R.drawable.check,
    };

    public interface GameEndListener {
        void onGameEnded();

        void victory();

        void winner();
    }
}

package com.bit.kokokokong;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.NonNull;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PlayAgainDialog extends Dialog {

    private OnClickListener onClickListener;
    private int rating;
    private String text;

    @BindView(R.id.txtScoreDialog)
    TextView txtScoreDialog;

    @BindView(R.id.text)
    TextView textView;

    PlayAgainDialog(@NonNull Context context, int rating, String text) {
        super(context, R.style.DialogTheme);
        this.rating = rating;
        this.text = text;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_play_again);
        ButterKnife.bind(this);
        txtScoreDialog.setText(getContext().getResources().getString(R.string.bitcoin, rating));
        textView.setText(text);
    }

    @OnClick(R.id.btnOk)
    void onClickOk() {
        if (onClickListener != null) {
            onClickListener.onClick(this, 0);
        }
    }

    public void setOnClickListener(OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }
}
